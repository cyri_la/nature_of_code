# this code is based on ytb video: https://www.youtube.com/watch?v=8ZEMLCnn8v0
#visualisation of perlin noise
import tkinter as tk
import random as R
import time as T
import math
#perlin noise class and function for x

class PerlinNoise:
    def __init__(self):
        self.grad = []
        self.permutation = []

        for _ in range(256):
            #generating random gradient vectors
            self.grad.append(R.uniform(-1, 1))
            
        #generate a random permutations of integers than duplicate
        self.permutation = R.sample(range(256), 256)*2
    #this makes the noise smooth
    def fade(self, t):
        return t*t*t*(t*(t*6 - 15) + 10)
    #linear interpolation between a and b using t
    def lerp(self, t, a, b):
        return a + t * (b - a)
    
    def grand_hash(self, hash_val, x):
        return self.grad[hash_val % 255] * x
    
    #generates the coordinates of x
    def noise(self, x):
        int_x = int(math.floor(x))
        frac_x = x - math.floor(x)

        x0 = int_x & 255
        x1 = (int_x + 1) & 255

        t = self.fade(frac_x)
        return self.lerp(t, self.grand_hash(self.permutation[x0],frac_x), self.grand_hash(self.permutation[x1], frac_x -1))

#canvas setup
    
width = 800
height = 600
geometryP = '' + str(width) + 'x' + str(height)
root =  tk.Tk()
root.geometry(geometryP)
root.title('Perlin Noise')

canvas = tk.Canvas(root, width = width, height = height, bg = 'white')
canvas.pack(anchor = tk.CENTER, expand = True)

r = 5
p = PerlinNoise()
t = 0.5
while(True):
    #x = R.randint(0,width)
    x = p.noise(t) * width
    #print(x)
    t = t + 0.01
    x = x + width/2
    canvas.create_oval(x + r, height/2 + r, x - r, height/2 - r, fill = 'black')
    # the point is in the middle of a circle
    canvas.update()
    T.sleep(0.01)
    canvas.delete('all')
    
