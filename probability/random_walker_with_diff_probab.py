#this code is based on video https://youtu.be/8uyR-YU_0dg
#basic code for randomised moving point -- which probability of motion is different 
import tkinter as tk
import random as r
#class for walker
class Walker:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def step(self):
        choice = r.random()
        print(choice)
        # 30% of going up
        if(choice < 0.3):
            self.y = self.y - 2
        # 20% of going down
        elif(choice < 0.5):
            self.y = self.y + 2
        # 20% of going right 
        elif(choice < 0.7):
            self.x = self.x + 2
        else:
            self.x = self.x - 2
    def get_x(self):
        return self.x
    def get_y(self):
        return self.y

        
#def draw_point(x,y):
    #canvas.create_rectangle((x,y),(x-1,y-1), width = 2, fill = 'red')

#canvas setup            
root =  tk.Tk()
root.geometry('800x600')
root.title('Random Walker')

canvas = tk.Canvas(root, width = 600, height = 400, bg = 'white')
canvas.pack(anchor = tk.CENTER,expand=True)

w =  Walker(200,200)

while(True):
    w.step()
    x = w.get_x()
    y = w.get_y()
    #canvas.after(10, draw_point(w.get_x(),w.get_y()))
    canvas.create_rectangle((x,y),(x-1,y-1), width = 2, fill = 'black')
    #root.mainloop()
    #draw_point(w.get_x(),w.get_y())
    canvas.update()

