#perlin noise 2D representation based on ytb video: https://youtu.be/ikwNrFvnL3g?list=PLRqwX-V7Uu6ZV4yEcW3uDwOgGXKUUsPOM
#note: this code will be updated for better image rendering - using the image pixels rather then canvas.create_line
import tkinter as tk
import random as R
import time as T
import math
#function to change rgb to hex as the outline accepts only that

def rgb_to_hex(rgb):
    r, g, b = rgb
    hex_color = "#{:02x}{:02x}{:02x}".format(r, g, b)
    #print(hex_color)
    return hex_color

#perlin noise function for x and y
class PerlinNoise:
    def __init__(self, dimension):
        self.grad = []
        self.permutation = []
        self.dimension = dimension

        if self.dimension ==1:
            for _ in range(256):
            #generating random gradient vectors
                self.grad.append(R.uniform(-1, 1))
        elif self.dimension == 2:
            for _ in range(256):
            #generating random gradient vectors
                angle = 2 * math.pi * R.random()
                self.grad.append([math.cos(angle), math.sin(angle)])
            
        #generate a random permutations of integers than duplicate
        self.permutation = R.sample(range(256), 256)*2
    #this makes the noise smooth
    def fade(self, t):
        return t*t*t*(t*(t*6 - 15) + 10)
    #linear interpolation between a and b using t
    def lerp(self, t, a, b):
        return a + t * (b - a)
    
    def grad_hash1D(self, hash_val, x):
        return self.grad[hash_val % 255] * x

    def grad_hash2D(self,hash_val,x,y):
        grad = self.grad[hash_val%255]
        return grad[0]*x + grad[1]*y
    
    #generates the perlin noise num for x
    def noise(self, x):
        int_x = int(math.floor(x))
        frac_x = x - math.floor(x)

        x0 = int_x & 255
        x1 = (int_x + 1) & 255

        t = self.fade(frac_x)

        noise_value = self.lerp(t, self.grad_hash(self.permutation[x0],frac_x), self.grad_hash(self.permutation[x1], frac_x -1))
        #normalizing the noise value to be from 0 - 1
        min_value = -1
        max_value = 1

        value_n = (noise_value - min_value)/ (max_value - min_value)
        return self.lerp(t, self.grad_hash(self.permutation[x0],frac_x), self.grad_hash(self.permutation[x1], frac_x -1))

    #generates the perlin noise for x,y in relation
    def noise(self, x, y):
        int_x = int(math.floor(x))
        int_y = int(math.floor(y))
        frac_x = x - int_x
        frac_y = y - int_y

        x0, x1 = int_x & 255, (int_x + 1)& 255
        y0, y1 = int_y & 255, (int_y + 1)& 255

        t_x, t_y = self.fade(frac_x), self.fade(frac_y)


        n00 =  self.grad_hash2D(self.permutation[self.permutation[x0] + y0], frac_x, frac_y)
        n01 =  self.grad_hash2D(self.permutation[self.permutation[x0] + y1], frac_x, frac_y -1)
        n10 =  self.grad_hash2D(self.permutation[self.permutation[x1] + y0], frac_x - 1, frac_y)
        n11 =  self.grad_hash2D(self.permutation[self.permutation[x1] + y1], frac_x -1, frac_y -1)
        lerp_x0 = self.lerp(t_x, n00, n10)
        lerp_x1 = self.lerp(t_x, n01, n11)
        
        noise_value = self.lerp(t_y, lerp_x0, lerp_x1)
        
        # normalizing the values to be from 0 - 1
        min_value = -1
        max_value = 1
        value_n = (noise_value - min_value)/(max_value - min_value)
        
        return value_n
        
#canvas setup
    
width = 200
height = 200
geometryP = '' + str(width) + 'x' + str(height)
root =  tk.Tk()
root.geometry(geometryP)
root.title('Perlin Noise')

canvas = tk.Canvas(root, width = width, height = height, bg = 'white')
canvas.pack(anchor = tk.CENTER, expand = True)


p = PerlinNoise(2)
#offsets
xoff = 0.5
yoff = 0.5;
while(True):
    for x in range(1, width):
        xoff = 0.5
        for y in range (1, height):
            xoff = xoff + 0.01
            # this uses random to generate color
            #c = R.randint(0,256)
            #this uses normalized perlin noise function to generate color
            c = int(p.noise(xoff, yoff)* 255)
            #print(c)
            canvas.create_line(x,y, x+1, y+1, fill = rgb_to_hex((c,c,c)))
            canvas.update()
        yoff = yoff + 0.01
            
    #canvas.mainloop()
    T.sleep(0.01)
    canvas.delete('all')
    
