#this code is based on video https://youtu.be/rqecAdEGW6I
#basic code for randomised moving point -- which probability of motion is same in all 4 directions

import tkinter as tk
import random as r
#class for walker

class Walker:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def step(self):
        choice = r.randint(1,4)
        print(choice)
        if(choice == 1):
            self.x = self.x - 2
        elif(choice == 2):
            self.y = self.y + 2
        elif(choice == 3):
            self.x = self.x + 2
        else:
            self.y = self.y - 2
    def get_x(self):
        return self.x
    def get_y(self):
        return self.y

#u can also use seperate fun for creating a point
        
#def draw_point(x,y):
    #canvas.create_rectangle((x,y),(x-1,y-1), width = 2, fill = 'red')

#canvas setup            
root =  tk.Tk()
root.geometry('800x600')
root.title('Random Walker')

canvas = tk.Canvas(root, width = 600, height = 400, bg = 'white')
canvas.pack(anchor = tk.CENTER, expand=True)

w =  Walker(200,200)

while(True):
    w.step()
    x = w.get_x()
    y = w.get_y()
    
    canvas.create_rectangle((x,y),(x-1,y-1), width = 2, fill = 'black')
    
    canvas.update()