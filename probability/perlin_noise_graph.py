#perlin noise graphing based on ytb video: https://www.youtube.com/watch?v=y7sgcFhk6ZM&list=PLRqwX-V7Uu6ZV4yEcW3uDwOgGXKUUsPOM&index=5

import tkinter as tk
import random as R
import time as T
import math
#perlin noise function for x

class PerlinNoise:
    def __init__(self):
        self.grad = []
        self.permutation = []

        for _ in range(256):
            #generating random gradient vectors
            self.grad.append(R.uniform(-1, 1))
            
        #generate a random permutations of integers than duplicate
        self.permutation = R.sample(range(256), 256)*2
    #this makes the noise smooth
    def fade(self, t):
        return t*t*t*(t*(t*6 - 15) + 10)
    #linear interpolation between a and b using t
    def lerp(self, t, a, b):
        return a + t * (b - a)
    
    def grand_hash(self, hash_val, x):
        return self.grad[hash_val % 255] * x
    
    #generates the coordinates of x
    def noise(self, x):
        int_x = int(math.floor(x))
        frac_x = x - math.floor(x)

        x0 = int_x & 255
        x1 = (int_x + 1) & 255

        t = self.fade(frac_x)
        return self.lerp(t, self.grand_hash(self.permutation[x0],frac_x), self.grand_hash(self.permutation[x1], frac_x -1))

#canvas setup
    
width = 800
height = 600
geometryP = '' + str(width) + 'x' + str(height)
root =  tk.Tk()
root.geometry(geometryP)
root.title('Perlin Noise')

canvas = tk.Canvas(root, width = width, height = height, bg = 'white')
canvas.pack(anchor = tk.CENTER, expand = True)


p = PerlinNoise()
#offsets
xoff = 0.5
#yoff = 10000;
while(True):
    #creating connected lines with xold and yold
    
    yold = p.noise(xoff) * height
    xold = 0
    for x in range(1, width):
        xoff = xoff + 0.01
        #y = R.randint(0,height) 
        y = height/2 - p.noise(xoff) * (height)
        canvas.create_line(xold,yold,x, y)
        xold = x
        yold = y

    canvas.update()
    T.sleep(0.01)
    canvas.delete('all')
    
