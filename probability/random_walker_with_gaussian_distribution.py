#this code is based on video https://www.youtube.com/watch?v=8uyR-YU_0dg
#basic code for randomised movement based on gaussian distribution - i am using higher probab for movement in up and down and lower probab in left to right

import tkinter as tk
import random as r
#class for walker
class Walker:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def step(self):
        choice = r.gauss()
        print(choice)
        if(choice < - 2):
            self.x = self.x - 2
        elif(choice >= -2 and choice < 0):
            self.y = self.y + 2
        elif(choice >= 2):
            self.x = self.x + 2
        else:
            self.y = self.y - 2
    def get_x(self):
        return self.x
    def get_y(self):
        return self.y

        
#def draw_point(x,y):
    #canvas.create_rectangle((x,y),(x-1,y-1), width = 2, fill = 'red')

#canvas setup            
root =  tk.Tk()
root.geometry('800x600')
root.title('Random Walker')

canvas = tk.Canvas(root, width = 600, height = 400, bg = 'white')
canvas.pack(anchor = tk.CENTER,expand=True)

w =  Walker(200,200)

while(True):
    w.step()
    x = w.get_x()
    y = w.get_y()
    #canvas.after(10, draw_point(w.get_x(),w.get_y()))
    canvas.create_rectangle((x,y),(x-1,y-1), width = 2, fill = 'black')
    #root.mainloop()
    #draw_point(w.get_x(),w.get_y())
    canvas.update()

