# this code is based on https://www.youtube.com/watch?v=DfziDXHYoik&list=PLRqwX-V7Uu6aFlwukCmDf0-1-uSR7mklK&index=5
# visual intepretation of custom distribution
import tkinter as tk
import random as r

def linearDistrb():
    done = FalseS
    hack = 0
    while (not done and hack < 10000):
        r1 = float(r.random())
        r2 = float(r.random())
        #print(r1, r2)
        y = r1*r1 #we can use only r1 as an y = x function, but x*x works too
        print(y)
        # if it is valid we use the r1
        if(r2 < y):
            done = True
            return r1
        #adding 1 to hack so we dont overdo
        hack = hack + 1
    return 0

#canvas setup            
root =  tk.Tk()
root.geometry('800x600')
root.title('Custom Distrib')

canvas = tk.Canvas(root, width = 600, height = 400, bg = 'white')
canvas.pack(anchor = tk.CENTER,expand=True)

#creating fields for values

vals = [0.0]*400
# index of spot we chose

while(True):
    n = linearDistrib()
    # index of spot we chose
    index = int(n*400)
    #increasing the num 
    vals[index] = vals[index] + 1
    print(n)
    canvas.create_rectangle((index,400),(index-1,400-vals[index]), width = 2, fill = 'black')
    #root.mainloop()
    
    canvas.update()
